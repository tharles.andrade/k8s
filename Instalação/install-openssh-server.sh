#!/bin/bash

apt install -y openssh-server 

sed -i 's,^PermitRootLogin no.*$,PermitRootLogin yes,' /etc/ssh/sshd_config

mkdir /home/aluno/.ssh

echo "Host *  StrictHostKeyChecking no " > /home/aluno/.ssh/config

sudo chmod 400 /home/aluno/.ssh/config

/etc/init.d/ssh restart 



